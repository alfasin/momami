package com.example.alfasi.momami;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;


public class MoMaMiActivity extends ActionBarActivity {

    final private static String TAG = "MoMaMi - Main Activity";
    private static Context mContext;
    private static int lastProgress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_mo_ma_mi);
        setActionBar();
        setSeekBarListerners();
    }

    private void setSeekBarListerners() {
        final SeekBar sk = (SeekBar) findViewById(R.id.seekbar);
        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "onProgressChanged: progress = " + progress +
                        "; lastProgress = " + lastProgress);

                if (lastProgress > progress) {
                    progress -= lastProgress;
                }
                lastProgress = progress;

                Log.d(TAG, "onProgressChanged: progress = " + progress +
                        "; lastProgress = " + lastProgress);

                TextView t1 = (TextView)findViewById(R.id.text1);
                TextView t2 = (TextView)findViewById(R.id.text2);
                TextView t3 = (TextView)findViewById(R.id.text3);
                TextView t4 = (TextView)findViewById(R.id.text4);
                TextView t5 = (TextView)findViewById(R.id.text5);

                // relax the progress a bit - it looks better on the device!
                if (progress % 2 == 0) {
                    progress /= 2;
                    t1.setBackgroundColor((((ColorDrawable) t1.getBackground()).getColor()) + progress);
                    t2.setBackgroundColor((((ColorDrawable) t2.getBackground()).getColor()) + progress);
                    t3.setBackgroundColor((((ColorDrawable) t3.getBackground()).getColor()) + progress);
                    t4.setBackgroundColor((((ColorDrawable) t4.getBackground()).getColor()) + progress);
                    t5.setBackgroundColor((((ColorDrawable) t5.getBackground()).getColor()) + progress);
                }
            }
        });
    }

    private void setActionBar() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        actionBar.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mo_ma_mi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (R.id.visit_moma == id) {
            Log.i(TAG, "I need to visit MoMa (online)");
            Intent intent = new Intent(mContext, MoMaMiWebActicity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
