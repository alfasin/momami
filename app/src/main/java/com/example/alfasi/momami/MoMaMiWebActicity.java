package com.example.alfasi.momami;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Created by alfasi on 7/9/15.
 */
public class MoMaMiWebActicity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.moma)
                .setTitle("Let's visit MoMa!")
                .setMessage("To be, or not to be -- that is the question: " +
                                "Whether 'tis nobler in the mind to suffer")
                .setPositiveButton(R.string.later_buddy, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.visit_moma, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        goToMoMa();
                    }
                })
                .show();


    }

    private void goToMoMa() {
        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://www.moma.org");
    }
}
